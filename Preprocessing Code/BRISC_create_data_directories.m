% BRISC_create_data_directories
% 
% This script creates directories for storing data for all of the EEG
% experiments
%
% Note: This will change the working directory to that which contains this
% script!
%
% Written by Daniel Feuerriegel, 1/19 at the University of Melbourne



% Change working directory to that which contains this script
mfile_name = mfilename('fullpath');
[pathstring, nameOfFile, extensionString]  = fileparts(mfile_name);
cd(pathstring);


% Make all of the relevant data directories

% Resting State
mkdir('Resting State/Data');
mkdir('Resting State/Data/Cleaned');
mkdir('Resting State/Data/Epoched');
mkdir('Resting State/Data/For Checking');
mkdir('Resting State/Data/Loaded');
mkdir('Resting State/Data/Parameters');

% Roving Oddball
mkdir('Roving Oddball/Data');
mkdir('Roving Oddball/Data/Cleaned');
mkdir('Roving Oddball/Data/Post ICA');
mkdir('Roving Oddball/Data/Epoched');
mkdir('Roving Oddball/Data/For Checking');
mkdir('Roving Oddball/Data/Loaded');
mkdir('Roving Oddball/Data/Parameters');

% SSVEPs
mkdir('SSVEP/Data');
mkdir('SSVEP/Data/Cleaned');
mkdir('SSVEP/Data/Post ICA');
mkdir('SSVEP/Data/Epoched');
mkdir('SSVEP/Data/For Checking');
mkdir('SSVEP/Data/Loaded');
mkdir('SSVEP/Data/Parameters');
