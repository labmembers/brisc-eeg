function Par = BRISC_resting_subject_codes(Par)
%
% Contains a list of subject ID codes, for preprocessing the BRISC EEG
% datasets. Enter the list of subject ID codes here, and they will be used
% by the relevant BRISC EEG preprocessing scripts
%
% Written by Daniel Feuerriegel, 1/19
%
% Note: Make sure to assign ID numbers to each subject code, as this is
% useful when batch processing the datasets. Some coordination may be
% required when multiple people are adding codes to this list!

Par.subjectCodesList = { ...
                  '1F0348', ... % ID 1
                  '1M0387', ... % ID 2 
                  '3F0422', ... % ID 3
                  '3F0425', ... % ID 4
                  '3M0374', ... % ID 5
                }; %IDs to analyse