function Par = BRISC_oddball_bad_channels(Par, datasetNo, EEG)
%
% Names/Numbers of bad channels are entered into this function. This is
% called by the relevant BRISC EEG preprocessing scripts to get these lists
% of noisy channels.
%
% Written by Daniel Feuerriegel, 1/19

Par.badChannelLabels = {};

% Set bad channels based on visual inspection or tester notes
switch [Par.subjectCodesList{datasetNo} '_', Par.testingPhase]

    case '1F0348_M'

        Par.badChannelLabels = {'xxxxx'};

    case '1M0387_M'

        Par.badChannelLabels = {'Fz', 'FC2'};

    case '3F0422_M'

        Par.badChannelLabels = {'Fz', 'FC1', 'FC2'};

    case '3F0425_M'

        Par.badChannelLabels = {'Fz', 'F3', 'Cz', 'FC2'};
        
    case '3M074_M'

        Par.badChannelLabels = {'FC1'};

end % of Par.subjectCodesList



% Get array of channel labels (for finding indices of bad channels)
for chanNo = 1:EEG.nbchan
    
    channelLabels{chanNo} = EEG.chanlocs(chanNo).labels;
    
end % of for chanNo

% Check if each entered bad channel is entered correctly (match with
% EEG.chanlocs entries)
if isempty(Par.badChannelLabels) == 0

    for badChanNo = 1:length(Par.badChannelLabels)
    
    if ismember(Par.badChannelLabels{badChanNo}, channelLabels);
            
    else
        
        error('Bad channel labels do not match labels in EEG dataset - entered incorrectly?');
        
    end % of if ismember
        
    end % of for badChanNo
        
end % of if isempty


% Reset bad channel indices vector
Par.badChannelIndices = [];


% Find indices of each bad channel (identified by the channel labels above)
if isempty(Par.badChannelLabels) == 0

    for badChanNo = 1:length(Par.badChannelLabels)
        
        Par.badChannelIndices(badChanNo) = find(strcmp(channelLabels, Par.badChannelLabels{badChanNo}));
        
    end % of for badChanNo

end % of if isempty








