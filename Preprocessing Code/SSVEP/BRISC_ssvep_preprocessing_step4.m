%% BRISC_ssvep_preprocessing.m

% Removes selected independent components and saves file. To be used with the
% BRISC EEG datasets. Relevant data processing parameters are set at the
% beginning of the script (in the Settings/Parameters section).
%
%
% First version written by Daniel Feuerriegel, 1/19 at the University of
% Melbourne.
%
%
% NOTE ------ IMPORTANT -------- Turn Option "Use memory mapped array under
% matlab 7x..." To OFF (unticked) in the EEGLab Memory and Other Options
% settings. Many functions (like interpolation) won't work properly unless
% this is disabled!
%
%
% NOTE ---- To avoid automatic conversion to single precision set EEGLab to
% NOT make two files (.fdt and .set) when saving data (uncheck the option)
% as this will automatically convert files to single precision.



%% Housekeeping
% Clear all variables in the workspace, and close all windows
clear all;
close all;

% Reset EEGLab settings to:
% - Not use memory mapped objects
% - Not use single precision (use double precision instead)
pop_editoptions( 'option_storedisk', 1, ...
    'option_savetwofiles', 0, ...
    'option_saveversion6', 0, ...
    'option_single', 0, ...
    'option_memmapdata', 0, ...
    'option_eegobject', 0, ...
    'option_computeica', 1, ...
    'option_scaleicarms', 0, ...
    'option_rememberfolder', 1, ...
    'option_donotusetoolboxes', 0, ...
    'option_checkversion', 0, ...
    'option_chat', 0);

% Change working directory to that which contains this script
mfile_name = mfilename('fullpath');
[pathstring, nameOfFile, extensionString]  = fileparts(mfile_name);
cd(pathstring);



%% Setting Parameters


% NOTE: Figure out where I input the trigger recording function, and how
% this will work for our different paradigms.


% Enter filepath of directory where the EEGLAB-processed EEG data is stored, relative to the
% current MATLAB working directory
Par.dataFolder = 'Data';

% Subject IDs to process (entry numbers from list of IDs in Par.subjectCodesList)
subjectIDsToDo = [3];


% Get array of subject ID codes
Par = BRISC_ssvep_subject_codes(Par);


% Choose experiment name ('bubbles' / 'sound' / 'movie'
Par.experimentName = 'movie';

% Select testing phase
% 'M' = midline
Par.testingPhase = 'M';

% Select whether to remove artefactual IC components from the dataset
Par.removeSelectedICs = 0; % 1 = Remove selected ICs / 0 = Don't remove

% Start and end times of baseline period (in ms from trigger onset)
Par.epochBaseline_StartEnd = [-100, 0]; 

% Epoch rejection amplitude thresholds (post ICA routine)
Par.epochRejectThreshold_strict = 100;
Par.epochRejectThreshold_moderate = 200;
Par.epochRejectThreshold_lenient = 300;

% Channels for inclusion in epoch rejection routine
Par.channelsForEpochRejection = [15:17]; % Oz, O1, O2



%% EEG Preprocessing Pipeline
    
for datasetNo = subjectIDsToDo

    % Announce the dataset that we are processing
    fprintf(['-----------------------------------------------------',  ...
        '\n\nProcessing subject code ' Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, ' ', Par.experimentName, ' dataset...\n\n', ...
        '-----------------------------------------------------\n\n']);

    % Record subject ID in Par structure
    Par.subjectID = Par.subjectCodesList{datasetNo};
    
    % Make another copy of subject code that can easily be located in the
    % workspace
    AAA_subjectID_Code = [Par.subjectID, '_', Par.testingPhase];
    
    
    
    %% -- Load Data File

    % Load EEG file with channel locations appended
    EEG = pop_loadset([Par.dataFolder, '/Post ICA/', Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, '_', Par.experimentName, '_ICA.set']);

    % Load the Par structure (with stored parameters) from Step 2
    % (Loaded into substructure so that Par structure isn't overwritten)
    Par.Step1 = load([Par.dataFolder, '/Parameters/', Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, '_', Par.experimentName, '_EEG_params_step1']);
    Par.Step2 = load([Par.dataFolder, '/Parameters/', Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, '_', Par.experimentName, '_EEG_params_step2']);
    Par.Step3 = load([Par.dataFolder, '/Parameters/', Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, '_', Par.experimentName, '_EEG_params_step3']);

    

    %% -- Mark Independent Components to Remove
    
    if Par.removeSelectedICs
    
        % Get vector of artefact ICs
        Par = BRISC_ssvep_ICs_to_remove(Par, datasetNo);
        
        % Remove the components
        EEG = pop_subcomp(EEG, Par.ICsToRemove, 0);
    
    end % of if Par.removeSelectedICs
    
    
        
    %% -- Interpolate Noisy Electrodes Using the Cleaned Data
    
    % Use bad channels vector defined in Par structure at step 2
    % Spherical spline interpolation
    
    if isempty(Par.Step2.Par.badChannelIndices) == 0 % If any bad channels have been identified
        
        EEG = pop_interp(EEG, Par.Step2.Par.badChannelIndices, 'spherical');

    end % of if length
    
    
    
    %% -- Baseline Correct Epochs
    
    % Baseline correct to prestimulus interval
    EEG = pop_rmbase(EEG, Par.epochBaseline_StartEnd);

    
    
    %% -- Final Artefact Rejection (After Cleaning Data)
    
    % Amplitude-based artefact rejection
    % Process 3 versions of the dataset (each with different artefact
    % rejection thresholds)
    
    % Strict cutoff
    EEG_strict = pop_eegthresh(EEG, 1, ...
        Par.channelsForEpochRejection, ...
        -Par.epochRejectThreshold_strict, ...
        Par.epochRejectThreshold_strict, ...
        Par.Step2.Par.epochStart_sec, Par.Step2.Par.epochEnd_sec, ...
        0, 1);
    
    % Moderate cutoff
    EEG_moderate = pop_eegthresh(EEG, 1, ...
        Par.channelsForEpochRejection, ...
        -Par.epochRejectThreshold_moderate, ...
        Par.epochRejectThreshold_moderate, ...
        Par.Step2.Par.epochStart_sec, Par.Step2.Par.epochEnd_sec, ...
        0, 1);

    % Lenient cutoff
    EEG_lenient = pop_eegthresh(EEG, 1, ...
        Par.channelsForEpochRejection, ...
        -Par.epochRejectThreshold_lenient, ...
        Par.epochRejectThreshold_lenient, ...
        Par.Step2.Par.epochStart_sec, Par.Step2.Par.epochEnd_sec, ...
        0, 1);
    
    

    %% -- Save the Datasets
    
    EEG_strict = pop_saveset( EEG_strict, 'filename', [Par.dataFolder, '/Cleaned/', Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, '_', Par.experimentName, '_Cleaned_Strict.set']);

    EEG_moderate = pop_saveset( EEG_moderate, 'filename', [Par.dataFolder, '/Cleaned/', Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, '_', Par.experimentName, '_Cleaned_Moderate.set']);

    EEG_lenient = pop_saveset( EEG_lenient, 'filename', [Par.dataFolder, '/Cleaned/', Par.subjectCodesList{datasetNo}, '_', Par.testingPhase, '_', Par.experimentName, '_Cleaned_Lenient.set']);
    
    clear EEG_strict;
    clear EEG_moderate;
    clear EEG_lenient;
    
    

    %% -- Save the Parameters File
    save([Par.dataFolder, '/Parameters/', Par.subjectCodesList{datasetNo}, '_', Par.experimentName, '_', Par.testingPhase, '_EEG_params_step4'], 'Par');


    
end % of for datasetNo 
