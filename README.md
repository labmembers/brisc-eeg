# brisc-eeg

Welcome to the BRISC EEG project repository! This repository contains code and documentation related to the EEG data analyses in the BRISC project. This repository covers the analyses for the Bangladesh data, and we may set up a separate repository for the Malawi data. 

This repository contains:

- EEG data processing and analysis scripts for each experiment. These will be updated regularly as more EEG datasets are processed.
- Notes created during the processing of EEG data processing, including any problems with the code, strange artefacts, or other things which require further checking.
- Documents outlining the EEG data processing procedures, and guidelines for implementing these.

**INCLUDE EXTRA INFORMATION HERE WHEN THE REPO IS POPULATED BY RELEVANT CODE AND DOCS **