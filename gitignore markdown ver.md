### macOS ###
# General
.DS_Store
.AppleDouble
.LSOverride

# Icon must end with two \r
Icon

# Thumbnails
._*

# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk

# Files in Data Directories

/Preprocessing Code/Datasets To Test Code/*
/Preprocessing Code/Resting State/Data/*
/Preprocessing Code/Roving Oddball/Data/*
/Preprocessing Code/SSVEP/Data/*